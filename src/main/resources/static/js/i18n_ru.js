const i18n = {
    "event.delay": "Повторить через",
    "event.description": "Описание",
    "event.repeatedly": "Повторять",
    "event.startDate": "Начало события",
    "event.type": "Тип события",
    "event.type.event": "Событие",
    "event.type.purchase": "Список покупок",
    "event.type.custom": "Пользовательский скрипт",
    "submit": "Отправить",
};