const timer = () => {
    const events = getEvents();

};

function getEvents() {
    const events = $(".js-event");
    let result = [];
    events.each((_, el) => {
        result.push({
            event: el,
            endDate: new Date($(el).find(".js-startTime").data("start-time")),
        })
    });

    result.sort((a, b) => a.endDate - b.endDate);

    return result;
}


timer();
