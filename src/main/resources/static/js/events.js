"use strict";

//todo add exit button
const eventEmptyForm = `\
<div class="card">
    <form action="/events" method="post">
        <input hidden name="_csrf" value="${genCsrfToken()}">
        <div class="form-group">
            <!--todo add abbreviation 25-->
            <label for="description">${i18n["event.description"]}</label>
            <input class="form-control" name="description" id="description"
                   placeholder="${i18n["event.description"]}" type="text" autofocus required>
        </div>
        <div class="form-group">
            <!--todo from db-->
            <label for="type">${i18n["event.type"]}</label>
            <select class="form-control" name="type" id="type">
                <option selected value="event">${i18n["event.type.event"]}</option>    
                <option value="purchase">${i18n["event.type.purchase"]}</option>    
                <option value="custom">${i18n["event.type.custom"]}</option>    
            </select>
        </div>
        <div class="form-group">
            <label for="startDate">${i18n["event.startDate"]}</label>
            <input class="form-control" name="startDate" id="startDate" 
                   type="datetime-local" value="${getLocalDateTimeNow()}">
            <input hidden name="timeZone" type="text" value="${getUserTimeZone()}">
        </div>
        <div class="form-group">
            <label for="delay">${i18n["event.delay"]}</label>
            <input class="form-control" name="delay" id="delay" type="time" value="00:05" disabled>
        </div>
        <div class="form-group form-check">
            <input class="form-check-input" name="repeatedly" id="repeatedly" 
                   type="checkbox" onchange="changeDisable(this, 'delay')">
            <label class="form-check-label" for="repeatedly">${i18n["event.repeatedly"]}</label>
        </div>
        <button class="button btn-primary" onclick="_countEventForm--" type="submit">${i18n["submit"]}</button>
    </form>
</div>`;

let _countEventForm = 0;

function addUserEvent() {
    if (_countEventForm > 0) return;

    $("#event-container").append(eventEmptyForm);
    _countEventForm++;
}

function getUserTimeZone() {
    return Intl.DateTimeFormat().resolvedOptions().timeZone;
}

function changeDisable(checkbox, otherElementId) {
    $("#" + otherElementId).prop('disabled', !checkbox.checked)
}

function getLocalDateTimeNow() {
    let date = new Date();
    date.setHours(date.getHours() + 1);

    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    const hour = date.getHours();
    const minute = date.getMinutes();

    const leadZero = (number) => number > 9 ? "" + number : "0" + number;

    return `${year}-${leadZero(month)}-${leadZero(day)}T${leadZero(hour)}:${leadZero(minute)}`;
}

function genCsrfToken() {
    return $("meta[name='_csrf']").attr("content");
}
