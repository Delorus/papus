package ru.sherb.papus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "event_type")
@Immutable
public class EventType {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eventTypeSequenceIdGenerator")
    @SequenceGenerator(name = "eventTypeSequenceIdGenerator", sequenceName = "event_type_id_seq", allocationSize = 1)
    private int id;

    @NotBlank
    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Column(name = "description")
    private String description;
}
