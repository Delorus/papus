package ru.sherb.papus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "participant")
public class Participant {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "participantSequenceIdGenerator")
    @SequenceGenerator(name = "participantSequenceIdGenerator", sequenceName = "participant_id_seq", allocationSize = 1)
    private int id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "project")
    private Project project;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user")
    private User user;
}
