package ru.sherb.papus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "product")
public class Product {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "productSequenceIdGenerator")
    @SequenceGenerator(name = "productSequenceIdGenerator", sequenceName = "product_id_seq", allocationSize = 1)
    private int id;

    @NotBlank
    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @PositiveOrZero
    @Column(name = "price", nullable = false)
    private BigDecimal price;
}
