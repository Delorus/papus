package ru.sherb.papus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "custom_event")
public class CustomEvent {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customEventSequenceIdGenerator")
    @SequenceGenerator(name = "customEventSequenceIdGenerator", sequenceName = "custom_event_id_seq", allocationSize = 1)
    private int id;

    @Column(name = "script")
    private String script;

    @ManyToOne(optional = false)
    @JoinColumn(name = "event")
    private Event event;
}
