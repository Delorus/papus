package ru.sherb.papus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.PositiveOrZero;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "purchase")
public class Purchase {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "purchaseSequenceIdGenerator")
    @SequenceGenerator(name = "purchaseSequenceIdGenerator", sequenceName = "purchase_id_seq", allocationSize = 1)
    private int id;

    @PositiveOrZero
    @Column(name = "count")
    private int count;

    @ManyToOne(optional = false)
    @JoinColumn(name = "product")
    private Product product;

    @ManyToOne(optional = false)
    @JoinColumn(name = "event")
    private Event event;
}
