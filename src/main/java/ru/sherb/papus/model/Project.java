package ru.sherb.papus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "project")
public class Project {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "projectSequenceIdGenerator")
    @SequenceGenerator(name = "projectSequenceIdGenerator", sequenceName = "project_id_seq", allocationSize = 1)
    private int id;

    @NotBlank
    @Column(name = "title", unique = true, nullable = false)
    private String title;

    @OneToMany(mappedBy = "project")
    private List<Event> events;

    @OneToMany(mappedBy = "project")
    private List<Participant> participants;
}
