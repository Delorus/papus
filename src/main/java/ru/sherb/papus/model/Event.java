package ru.sherb.papus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.Instant;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "event")
public class Event {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eventSequenceIdGenerator")
    @SequenceGenerator(name = "eventSequenceIdGenerator", sequenceName = "event_id_seq", allocationSize = 1)
    private int id;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "start_time")
    private Instant startTime;

    @Column(name = "repeatedly", nullable = false)
    private boolean repeatedly;

    @Column(name = "delay_sec")
    private Long delaySec;

    @Column(name = "done", nullable = false)
    private boolean done;

    @Column(name = "overdue", nullable = false)
    private boolean overdue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "[user]")
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "type")
    private EventType type;
}
