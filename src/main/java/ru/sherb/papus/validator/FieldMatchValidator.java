package ru.sherb.papus.validator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.function.Function;

@Slf4j
public final class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {

    private String message;
    private String[] fields;

    private static Function<String, Object> createMappingFunction(Object obj) {
        return (fieldName) -> {
            Class<?> objClass = obj.getClass();
            Object value;
            try {
                value = objClass.getField(fieldName).get(obj);
            } catch (IllegalAccessException | NoSuchFieldException ignored) {
                try {
                    value = objClass.getMethod("get" + StringUtils.capitalize(fieldName))
                            .invoke(obj);
                } catch (IllegalAccessException
                        | InvocationTargetException
                        | NoSuchMethodException e) {

                    log.error(e.getMessage(), e);
                    throw new RuntimeException("not found field by name " + fieldName);
                }
            }
            return value;
        };
    }

    @Override
    public void initialize(FieldMatch annotation) {
        message = annotation.message();
        fields = annotation.value();
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {

        Function<String, Object> fieldNameToValue = createMappingFunction(obj);
        long uniqueField = Arrays.stream(fields)
                .map(fieldNameToValue)
                .distinct()
                .count();

        boolean valid = uniqueField == 1;

        if (!valid) {
            context.buildConstraintViolationWithTemplate(message)
                    .addPropertyNode(fields[0])
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }

        return valid;
    }
}
