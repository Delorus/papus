package ru.sherb.papus.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ru.sherb.papus.service.EventService;

@Component
public class UserLoginListener implements ApplicationListener<AuthenticationSuccessEvent> {

    private final EventService eventService;

    @Autowired
    public UserLoginListener(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent successEvent) {
        UserDetails userDetails = (UserDetails) successEvent.getAuthentication().getPrincipal();

        eventService.reloadEventsByUserLogin(userDetails.getUsername());
    }
}
