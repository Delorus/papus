package ru.sherb.papus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sherb.papus.model.Event;
import ru.sherb.papus.model.User;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {

    @Query("from Event e where e.overdue = false and e.user = :user")
    List<Event> getActiveByUser(@Param("user") User user);
}
