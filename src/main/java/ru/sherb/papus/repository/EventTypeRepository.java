package ru.sherb.papus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sherb.papus.model.EventType;

@Repository
public interface EventTypeRepository extends JpaRepository<EventType, Integer> {

    @Query("from EventType et where et.name = :name")
    EventType getByName(@Param("name") String name);
}
