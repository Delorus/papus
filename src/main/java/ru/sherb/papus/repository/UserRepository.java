package ru.sherb.papus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sherb.papus.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query("from User u where u.login = :login")
    User getByLogin(@Param("login") String login);
}
