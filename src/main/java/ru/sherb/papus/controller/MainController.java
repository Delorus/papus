package ru.sherb.papus.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.sherb.papus.repository.UserRepository;

@Slf4j
@Controller
public final class MainController {

    @Autowired
    @Lazy
    private UserRepository userRepository;


    @GetMapping({"/", "/index"})
    public String index(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "welcome";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }
}
