package ru.sherb.papus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sherb.papus.dto.NewEventFormDto;
import ru.sherb.papus.model.Event;
import ru.sherb.papus.model.User;
import ru.sherb.papus.service.EventService;
import ru.sherb.papus.service.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/events")
public final class EventController {

    private final EventService eventService;
    private final UserService userService;

    @Autowired
    public EventController(EventService eventService, UserService userService) {
        this.eventService = eventService;
        this.userService = userService;
    }

    @ModelAttribute("events")
    public List<Event> events(Principal principal, Model model) {
        eventService.reloadEventsByUserLogin(principal.getName());
        User user = userService.getByLogin(principal.getName());
        model.addAttribute("user", user);
        return user.getEvents();
    }

    @GetMapping
    public String eventList() {
        return "events";
    }

    @PostMapping
    public String addEvent(@ModelAttribute User user,
                           @ModelAttribute @Valid NewEventFormDto eventDto,
                           BindingResult result) {

        if (result.hasErrors()) {
            result.reject("message");
            return "redirect:/events"; //todo
        }

        Event event = eventService.createEvent(eventDto, user);
        eventService.scheduleEvent(event);
        return "redirect:/events";
    }
}
