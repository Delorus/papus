package ru.sherb.papus.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sherb.papus.dto.RegistrationDto;
import ru.sherb.papus.service.UserService;

import javax.persistence.EntityExistsException;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping(value = "/register")
public final class RegistrationController {

    private final UserService users;

    @Autowired
    public RegistrationController(UserService users) {
        this.users = users;
    }

    @ModelAttribute("user")
    public RegistrationDto registrationDto() {
        return new RegistrationDto();
    }

    @GetMapping
    public String show() {
        return "register";
    }

    @PostMapping
    public String registration(@ModelAttribute("user") @Valid RegistrationDto user, BindingResult result) {
        try {
            if (!result.hasErrors()) {
                users.registration(user);
            }
        } catch (EntityExistsException e) {
            result.rejectValue("login", "user.error.userAlreadyExist");
        }

        if (result.hasErrors()) {
            return "/register";
        }
        return "redirect:/login";
    }

}
