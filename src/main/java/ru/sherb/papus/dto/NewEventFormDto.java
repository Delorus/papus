package ru.sherb.papus.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDateTime;

import static org.springframework.format.annotation.DateTimeFormat.ISO;

@Data
public final class NewEventFormDto {

    @NotBlank
    private String description;

    @NotBlank
    private String type;

    @Future
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private LocalDateTime startDate;

    private String timeZone;

    private boolean repeatedly;

    @PositiveOrZero
    private Long delay;
}
