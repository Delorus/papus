package ru.sherb.papus.dto;

import lombok.Data;
import ru.sherb.papus.validator.FieldMatch;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@FieldMatch.List({
        @FieldMatch(value = {"password", "confirmPassword"}, message = "{user.error.passwordNotMatch}"),
        @FieldMatch(value = {"email", "confirmEmail"}, message = "{user.error.emailNotMatch}")
})
public final class RegistrationDto {

    @NotBlank
    String login;

    @NotBlank
    String password;

    @NotBlank
    String confirmPassword;

    String phone;

    @Email
    String email;

    String confirmEmail;
}
