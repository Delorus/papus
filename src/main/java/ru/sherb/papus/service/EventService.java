package ru.sherb.papus.service;

import ru.sherb.papus.dto.NewEventFormDto;
import ru.sherb.papus.model.Event;
import ru.sherb.papus.model.User;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface EventService {

    boolean scheduleEvent(@NotNull Event event);

    Event createEvent(@NotNull NewEventFormDto eventDto, User owner);

    List<Event> getActiveEventByLogin(String login);

    List<Event> finishEvent(Event... finishedEvents);

    List<Event> finishEvent(Iterable<Event> finishedEvents);

    List<Event> reloadEventsByUserLogin(String userLogin);
}
