package ru.sherb.papus.service.scheduler;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class Scheduler {

    private final ScheduledExecutorService scheduler;

    private final Map<Integer, ScheduledFuture> currentTasks = new HashMap<>();

    public Scheduler() {
        AtomicInteger count = new AtomicInteger(0);
        this.scheduler = Executors.newScheduledThreadPool(
                Runtime.getRuntime().availableProcessors(),
                (task) -> new Thread(null, task, "scheduled-thread-" + count.getAndIncrement())
        );
    }

    public boolean scheduleEvent(IEventAction action, Integer taskId, long delay) {
        if (currentTasks.containsKey(taskId)) {
            return false;
        }

        ScheduledFuture<?> task = scheduler.schedule(action, delay, TimeUnit.MILLISECONDS);
        currentTasks.put(taskId, task);

        return true;
    }

    public boolean eventIsDone(Integer eventId) {
        ScheduledFuture future = currentTasks.get(eventId);
        if (future == null) {
            return true;
        }

        if (future.isDone()) {
            currentTasks.remove(eventId);
            return true;
        }

        return false;
    }
}
