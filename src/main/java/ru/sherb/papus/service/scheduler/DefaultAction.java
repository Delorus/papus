package ru.sherb.papus.service.scheduler;

import ru.sherb.papus.model.Event;
import ru.sherb.papus.repository.EventRepository;

public final class DefaultAction implements IEventAction {

    private final EventRepository repository;
    private final int eventId;

    public DefaultAction(EventRepository repository, Integer eventId) {
        this.eventId = eventId;
        this.repository = repository;
    }

    @Override
    public void run() {
        Event event = repository.getOne(eventId);
        event.setDone(true);
        repository.save(event);
    }
}
