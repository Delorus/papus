package ru.sherb.papus.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sherb.papus.model.EventType;
import ru.sherb.papus.repository.EventTypeRepository;
import ru.sherb.papus.service.EventTypeService;

@Service
public class EventTypeServiceImpl implements EventTypeService {

    private final EventTypeRepository repository;

    @Autowired
    public EventTypeServiceImpl(EventTypeRepository repository) {
        this.repository = repository;
    }

    //todo
    @Override
    public EventType getTypeByName(String typeName) {
        return repository.getByName(typeName);
    }
}
