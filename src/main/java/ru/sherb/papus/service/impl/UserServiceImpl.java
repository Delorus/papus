package ru.sherb.papus.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.sherb.papus.dto.RegistrationDto;
import ru.sherb.papus.model.User;
import ru.sherb.papus.model.UserRoles;
import ru.sherb.papus.repository.UserRepository;
import ru.sherb.papus.service.UserService;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository, @Lazy PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public User registration(@NotNull RegistrationDto dto) {
        User existUser = repository.getByLogin(dto.getLogin());
        if (existUser != null) {
            throw new EntityExistsException("user.error.userAlreadyExist");
        }
        User user = new User();
        user.setLogin(dto.getLogin());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setEmail(dto.getEmail()); //todo unique
        user.setPhone(dto.getPhone());
        user.setRole(UserRoles.USER);
        return repository.saveAndFlush(user);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User getByLogin(@NotBlank String login) {
        User user = repository.getByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException("user.error.loginNotFound");
        }

        return user;
    }

    @Override
    //todo проверить что работает NotBlank
    public UserDetails loadUserByUsername(@NotBlank String username) throws UsernameNotFoundException {
        User user = repository.getByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException("user.error.loginNotFound");
        }
        return org.springframework.security.core.userdetails.User.builder()
                .username(user.getLogin())
                .password(user.getPassword())
                .authorities(user.getRole())
                .build();
    }
}
