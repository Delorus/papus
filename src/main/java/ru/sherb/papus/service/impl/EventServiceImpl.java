package ru.sherb.papus.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sherb.papus.dto.NewEventFormDto;
import ru.sherb.papus.model.Event;
import ru.sherb.papus.model.EventType;
import ru.sherb.papus.model.User;
import ru.sherb.papus.repository.EventRepository;
import ru.sherb.papus.repository.EventTypeRepository;
import ru.sherb.papus.repository.UserRepository;
import ru.sherb.papus.service.EventService;
import ru.sherb.papus.service.scheduler.DefaultAction;
import ru.sherb.papus.service.scheduler.Scheduler;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    private final Scheduler scheduler;
    private final EventRepository eventRepository;
    private final EventTypeRepository eventTypeRepository;
    private final UserRepository userRepository;

    @Autowired
    public EventServiceImpl(Scheduler scheduler,
                            EventRepository eventRepository,
                            EventTypeRepository eventTypeRepository,
                            UserRepository userRepository) {

        this.scheduler = scheduler;
        this.eventRepository = eventRepository;
        this.eventTypeRepository = eventTypeRepository;
        this.userRepository = userRepository;
    }

    @Override
    public boolean scheduleEvent(@NotNull Event event) {

        DefaultAction executor = new DefaultAction(eventRepository, event.getId());

        long deadline = event.getStartTime().truncatedTo(ChronoUnit.MINUTES).toEpochMilli();
        long delay = deadline - System.currentTimeMillis();
        //todo add support repeatedly prop
        return scheduler.scheduleEvent(executor, event.getId(), delay);
    }

    @Override
    @Transactional
    public Event createEvent(@NotNull NewEventFormDto eventDto, User owner) {
        Event result = new Event();

        result.setDescription(eventDto.getDescription());

        EventType type = eventTypeRepository.getByName(eventDto.getType()); //todo validation
        result.setType(type);

        LocalDateTime startDate = eventDto.getStartDate();
        if (startDate != null) {
            ZoneId zoneId = ZoneId.of(eventDto.getTimeZone());
            result.setStartTime(startDate.atZone(zoneId).toInstant());
        }

        result.setRepeatedly(eventDto.isRepeatedly());

        if (eventDto.isRepeatedly()) {
            result.setDelaySec(eventDto.getDelay());
        }

        if (owner != null) {
            result.setUser(owner);
        }

        eventRepository.save(result);

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> getActiveEventByLogin(String login) {
        User user = userRepository.getByLogin(login);
        return eventRepository.getActiveByUser(user);
    }

    @Override
    public List<Event> finishEvent(Event... finishedEvents) {
        return finishEvent(Arrays.asList(finishedEvents));
    }

    @Override
    public List<Event> finishEvent(Iterable<Event> finishedEvents) {
        for (Event event : finishedEvents) {
            event.setDone(true);
        }
        return eventRepository.saveAll(finishedEvents);
    }

    /**
     * @return active events that have not yet been scheduled
     */
    @Override
    @Transactional
    public List<Event> reloadEventsByUserLogin(String userLogin) {
        Instant now = Instant.now();

        List<Event> finishedEvents = new ArrayList<>();
        List<Event> activeEvents = new ArrayList<>();

        for (Event event : getActiveEventByLogin(userLogin)) {
            if (event.getStartTime().isBefore(now)) {
                finishedEvents.add(event);
                continue;
            }

            if (scheduleEvent(event)) {
                activeEvents.add(event);
            }
        }

        finishEvent(finishedEvents);

        return activeEvents;
    }
}
