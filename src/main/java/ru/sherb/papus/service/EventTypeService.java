package ru.sherb.papus.service;

import ru.sherb.papus.model.EventType;

public interface EventTypeService {

    EventType getTypeByName(String typeName);
}
