package ru.sherb.papus.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.sherb.papus.dto.RegistrationDto;
import ru.sherb.papus.model.User;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface UserService extends UserDetailsService {

    User registration(@NotNull RegistrationDto user);

    List<User> findAll();

    User getByLogin(@NotBlank String login);
}
